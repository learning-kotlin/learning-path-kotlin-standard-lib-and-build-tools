package org.elu.kotlin.leaning.stdlib

fun main(args: Array<String>) {
    // below list is loaded eagerly
    val elements = 1..10000
    val output = elements.filter { it < 30 }.map { Pair("Yes 1", it) }
    output.forEach {
        println(it)
    }

    // lazy collection example
    val elements2 = 1..100000000
    val output2 = elements2.asSequence().filter { it < 30 }.map { Pair("Yes 2", it) }
    output2.forEach {
        println(it)
    }

    val elements3 = 1..100000000
    val output3 = elements3.asSequence().take(30).sum()
    println(output3)

    val numbers = generateSequence(1) { x -> x + 10 }
    println(numbers.take(30).sum())

    val lazyInt: Int by lazy { 10 }
}
