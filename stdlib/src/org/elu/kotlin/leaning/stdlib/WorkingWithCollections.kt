package org.elu.kotlin.leaning.stdlib

fun main(args: Array<String>) {
    // following line have a problem; List is immutable while ArrayList is mutable
    val list: List<String> = ArrayList()

    // here's how list are defined and initialised in Kotlin
    val lista = listOf("First Entry", "Second Entry")
    val emptyList = emptyList<String>()
    val numbers = 1..100

    // immutable list
    val cities = listOf("Madrid", "Paris", "London")
    println(cities.javaClass)
    println(emptyList.javaClass)

    // mutable list
    val mutableCities = mutableListOf("Madrid")
    mutableCities.add("Paris")
    mutableCities.add("London")
    println(mutableCities.javaClass)
    println(mutableCities)

    // maps
    val hashMap = hashMapOf(Pair("Madrid", "Spain"), Pair("Paris", "France"))
    println(hashMap.javaClass)
    println(hashMap)

    val booleans = booleanArrayOf(true, false, true)
}
